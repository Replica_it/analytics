import sys
import os

from data_maining import get_dataframe_shape

repo_dir = os.path.expanduser('~/analytics')

sys.path.append(repo_dir)

file_path = '~/shared/order_leads.csv'
data_shape = get_dataframe_shape(file_path)
# Выведет кол-во строк и колонок
print(data_shape)
