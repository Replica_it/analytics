import pandas as pd


def get_dataframe_shape(file_path):
    """
    :param file_path: dataframe
    :return: shape
    """
    df = pd.read_csv(file_path)
    return df.shape


def get_dataframe_dtypes(file_path):
    """
    :param file_path: dataframe
    :return: type
    """
    df = pd.read_csv(file_path)
    return df.dtypes
